//Soal 1
//Jawaban Soal 1

console.log("\nJawaban Soal 1\n") ; 

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()
console.log(daftarHewan)


//Soal 2

//Jawaban Soal 2

console.log("\nJawaban Soal 2\n") ; 

function introduce({name , age , address , hobby}) {
    return "Nama saya " + name + ", umur saya " + age + " tahun," + " alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby
}
    
    
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
     
var perkenalan = introduce(data)

 
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"


//Soal 3
//Jawaban Soal 3

console.log("\nJawaban Soal 3\n") ; 

function hitung_huruf_vokal(str) {
    menghitung = str.match(/[aeiou]/gi).length
    return menghitung
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


//Soal 4
//Jawaban Soal 4

console.log("\nJawaban Soal 4\n") ; 

function hitung(nilai){
    return (nilai * 2) - 2 ;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
