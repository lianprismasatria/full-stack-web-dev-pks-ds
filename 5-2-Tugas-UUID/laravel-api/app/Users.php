<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Users extends Model
{
    protected $fillable = [ 'username', 'email', 'name' , 'id', 'roles_id', 'password', 'email_verified_at' ];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected $hidden = ['remember_token'];

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if (empty ($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function roles()
    {
        return $this->hasOne('App\Roles');
    }

    public function otp_code(){

        return $this->hasOne('App\OtpCode');
    }
}
