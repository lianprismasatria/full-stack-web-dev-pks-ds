<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = ['name' , 'id'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();


        static::creating(function($model){
            if (empty ($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }

    public function users()
    {
        return $this->belongsTo('App\Users');
    }

}
