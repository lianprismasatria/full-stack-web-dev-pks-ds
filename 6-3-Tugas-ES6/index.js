//Jawaban Soal 1
const persegipanjang = (panjang, lebar) => {
    let luas = panjang*lebar
    let keliling = 2*panjang + 2*lebar
    console.log(`Luas: ${luas}\nKeliling: ${keliling}`)
};
persegipanjang(4,3);


//Jawaban Soal 2
const newFunction = (firstName, lastName) => {
    return {
      fullName(){
        console.log(firstName + " " + lastName)
      }
    }
  };
newFunction("William", "Imoh").fullName() ;


//Jawaban Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  };
const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);


//Jawaban Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west,...east]
console.log(combined)


//Jawaban Soal 5
const planet = "earth" 
const view = "glass" 
let after = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet}`;
console.log(after)