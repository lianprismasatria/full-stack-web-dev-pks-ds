<!DOCTYPE html> 
<html lang="en"> 
 
<head> 
<meta charset="UTF-8"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta http-equiv="X-UA-Compatible" content="ie=edge"> 
<title>Elang vs Harimau</title> 
</head> 
<body> 
<?php 
 
trait Fight { 
    public $attackPower; 
    public $defensePower; 
 
    public function serang($lawan) { 
        if ($lawan->darah <= 0) { 
            echo $lawan->nama . " kalah <br>"; 
 
        } else { 
            echo $this->nama . " menyerang " . $lawan->nama . "<br>"; 
            return $lawan->diserang($this); 
        } 
    } 
 
    public function diserang($penyerang) { 
        echo $this->nama . " diserang" . "<br>"; 
        $this->darah -= ($penyerang->attackPower/$this->defensePower); 
 
        echo "Darah " . $this->nama . " sekarang " . $this->darah . "<br>"; 
 
        if ($this->darah <= 0) { 
            echo $this->nama . " kalah <br>"; 
        } 
 
    } 
} 
 
abstract class Hewan { 
    use Fight; 
    public $nama; 
    public $jumlahKaki; 
    public $keahlian = ""; 
    public $darah = 50; 
    public function __construct($nama) { 
        $this->nama = $nama; 
    } 
 
    public function atraksi() { 
        echo $this->nama . " sedang " . $this->keahlian . "<br>"; 
    } 
} 
 
class Elang extends Hewan { 
    public $jumlahKaki = 2; 
    public $keahlian = "terbang tinggi"; 
    public $attackPower = 10; 
    public $defensePower = 5; 
 
    public function getInfoHewan() { 
        echo "Nama Hewan: " . $this->nama . "<br> Jenis Hewan: " . get_class($this)  
        . "<br> Jumlah kaki: " . $this->jumlahKaki . "<br> Keahlian: " . $this->keahlian  
        . "<br> Sisa darah: " . $this->darah . "<br> Attack Power: " . $this->attackPower . "<br> Defense Power: " . $this->defensePower . "<br>"; 
    } 
} 
 
class Harimau extends Hewan { 
    public $jumlahKaki = 4; 
    public $keahlian = "Lari cepat"; 
    public $attackPower = 7; 
    public $defensePower = 8; 
 
    public function getInfoHewan() { 
        echo "Nama Hewan: " . $this->nama . "<br> Jenis Hewan: " . get_class($this)  
        . "<br> Jumlah kaki: " . $this->jumlahKaki . "<br> Keahlian: " . $this->keahlian  
        . "<br> Sisa darah: " . $this->darah . "<br> Attack Power: " . $this->attackPower . "<br> Defense Power: " . $this->defensePower . "<br>"; 
    } 
} 
 
// Test Tampil 
echo "<h3>Animal Fighter</h3>"; 
 
$elang = new Elang("Elang"); 
$harimau = new Harimau("Harimau"); 
 
echo "Nama: " . $elang->nama . "<br>";  
echo "Jumlah Kaki: " . $elang->jumlahKaki . "<br>";  
echo "Keahlian: " . $elang->keahlian . "<br> <br>";  
 
echo "Nama: " . $harimau->nama . "<br>";  
echo "Jumlah Kaki: " . $harimau->jumlahKaki . "<br>";  
echo "Keahlian: " . $harimau->keahlian . "<br> <br>";  
 
$elang->atraksi(); 
echo "<br>"; 
$elang->getInfoHewan(); 
echo "<br>"; 
$harimau->atraksi(); 
echo "<br>"; 
$harimau->getInfoHewan();
echo "<br>";
echo "<h3>Battle Fight</h3>";  
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang); 
$elang->serang($harimau);
$harimau->serang($elang); 
$elang->serang($harimau); 
$harimau->serang($elang);

 
?> 
 
</body> 
 
</html>
